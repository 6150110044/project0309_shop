package com.example.login;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class point extends AppCompatActivity {

    TextView tv2,tv3 ;
    Button btn1 ;
    ImageView img1,img2,img3, imghome,imgout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_point);

        img1 =findViewById(R.id.member_d);
        img2 = findViewById(R.id.scanqr2);
        img3 = findViewById(R.id.shop_d);
        imghome = findViewById(R.id.home_d);
        imgout = findViewById(R.id.logout_d);

        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(point.this,home.class);
                startActivity(i);
            }
        });

        imgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,MainActivity.class);
                startActivity(intent);

            }
        });


        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,Profile.class);
                startActivity(intent);
            }
        });

        imgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,MainActivity.class);
                startActivity(intent);
            }
        });

        btn1 =findViewById(R.id.search_d5);

         tv3 =(TextView) findViewById(R.id.text_d4);
         tv2 =  (TextView) findViewById(R.id.text_d3);
        TextView tv1 = (TextView) findViewById(R.id.text_d);
        Intent i = this.getIntent();
        String result = i.getExtras().getString("QRCODE");
        tv1.setText(result);

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,MemberDetail.class);
                startActivity(intent);
            }
        });

        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,ScanQR.class);
                startActivity(intent);
            }
        });

        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(point.this,Profile.class);
                startActivity(intent);
            }
        });

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Double point = Double.valueOf(tv2.getText().toString())/50;
                tv3.setText(String.valueOf(point));

            }
        });


    }

}