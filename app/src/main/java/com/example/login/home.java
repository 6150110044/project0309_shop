package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class home extends AppCompatActivity {
    ImageView imMem,imScan,imPro,imghome,imgout ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        imMem = findViewById(R.id.Member_Im);
        imScan = findViewById(R.id.Scan_Im);
        imPro = findViewById(R.id.profile_Im);
        imghome = findViewById(R.id.imageView);
        imgout =findViewById(R.id.imageView2);

        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home.this,home.class);
                startActivity(intent);

            }
        });

        imgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home.this,MainActivity.class);
                startActivity(intent);

            }
        });

        imMem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(home.this,MemberDetail.class);
                startActivity(intent);
            }
        });

        imScan.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(home.this,ScanQR.class);
                startActivity(intent);

            }
        });

        imPro.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(home.this,Profile.class);
                startActivity(intent);

            }
        });

    }
}