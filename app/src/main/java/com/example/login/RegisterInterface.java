package com.example.login;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;

public interface RegisterInterface {
    public static String BASE_URL = "https://itlearningcenters.com/android/project0309/";
    @FormUrlEncoded
    @POST("regist_shop.php")
    public Call<ResponseBody> registerUser(@Field("shop_id") String shopIDValue,
                                           @Field("username") String userNameValue,
                                           @Field("password") String passwordValue,
                                           @Field("shopname") String shopnameValue,
                                           @Field("email") String emailValue,
                                           @Field("phone") String phoneValue,
                                           @Field("address") String addressValue);


    //    public static String LogBASE_URL = "http://10.0.2.2/register/";
    @FormUrlEncoded
    @POST("shop_login.php")
    Call<ResponseBody> login( @Field("username") String userNameValue,
                              @Field("password") String passwordValue);

//    public static final String BASE_URLSHOP = "https://itlearningcenters.com/android/project0309/";
    @GET("list_shops.php")
    Call<List<loginresponse>> getShop();

    @GET("cust_profile.php")
    Call<CustProfile> getCust();


}
