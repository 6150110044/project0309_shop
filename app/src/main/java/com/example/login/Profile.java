package com.example.login;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.Field;

import static android.Manifest.permission.CAMERA;

public class Profile extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 100;
    private static String[] PERMISSIONS_CAMERA = {CAMERA};

    public static TextView tvresult;
    private Button btnscan;
    private Button btngenqr;
    ImageView img1, img2,img3,imghome,imgout ;
    TextView txtid,txtuser,txtshop,txtemail,txtphone,txtaddress ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        txtid =findViewById(R.id.textView5);
        txtuser =findViewById(R.id.textView6);
        txtshop =findViewById(R.id.textView9);
        txtemail =findViewById(R.id.textView19);
        txtphone =findViewById(R.id.textView20);
        txtaddress =findViewById(R.id.textView21);

//        String shopIDValue = null;
//        String userNameValue = null ;
//        String shopnameValue = null;
//        String emailValue = null ;
//        String phoneValue = null;
//        String addressValue = null ;


        img1 = findViewById(R.id.Member_Im);
        img2 = findViewById(R.id.profile);
        img3 = findViewById(R.id.scanqr);

        imghome = findViewById(R.id.imageView);
        imgout = findViewById(R.id.imageView2);

        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Profile.this,home.class);
                startActivity(i);
            }
        });

        imgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this,MainActivity.class);
                startActivity(intent);

            }
        });



        img1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this,MemberDetail.class);
                startActivity(intent);
            }
        });

        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Profile.this,ScanQR.class);
                startActivity(intent);
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        Toast.makeText(getApplicationContext(), "Permission already granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        requestPermission(Profile.this);
                    }
                }
            }
        });


        Retrofit retrofit = new  Retrofit.Builder()
                                .baseUrl(RegisterInterface.BASE_URL).addConverterFactory(GsonConverterFactory.create())
                                .build();

        RegisterInterface registerInterface = retrofit.create(RegisterInterface.class);
      Call<List<loginresponse>> call = registerInterface.getShop();
      call.enqueue(new Callback<List<loginresponse>>() {
          @Override
          public void onResponse(Call<List<loginresponse>> call, Response<List<loginresponse>> response) {


            List<loginresponse> loginresponseList = response.body();



//              String[] customers = new String[loginresponseList.size()];
              //7. looping through all the customers and inserting the names inside the string array
              for (int i = 0; i < loginresponseList.size();  i++) {

                  txtid.setText( loginresponseList.get(i).getId());
                      txtuser.setText(loginresponseList.get(i).getUsername());
                      txtshop.setText( loginresponseList.get(i).getShopname());
                      txtemail.setText( loginresponseList.get(i).getEmail());
                      txtphone.setText( loginresponseList.get(i).getPhonenumber());
                      txtaddress.setText( loginresponseList.get(i).getAddress());

              }

          }

          @Override
          public void onFailure(Call<List<loginresponse>> call, Throwable t) {

          }
      });




    }
    private boolean checkPermission() {
        // Check if we have permission
        return (ContextCompat.checkSelfPermission(
                getApplicationContext(),
                CAMERA) == PackageManager.PERMISSION_GRANTED
        );
    }
    private void requestPermission(Activity activity) {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_CAMERA,
                REQUEST_CAMERA
        );
    }



}