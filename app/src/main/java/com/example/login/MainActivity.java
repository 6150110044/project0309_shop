package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.util.List;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

    private static String TAG_RETROFIT_GET_POST = "RETROFIT_GET_POST";
    private ProgressDialog progressDialog = null;
    Button btn1, btn2;

    EditText userNameEditText;
    EditText passwordEditText;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);




        btn2 = findViewById(R.id.register_button);
        btn2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MainActivity.this, Register.class);
                startActivity(intent);
            }
        });

        btn1 = findViewById(R.id.btnLogin);
        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                userNameEditText = findViewById(R.id.editusername);
                passwordEditText = findViewById(R.id.editpassword);
                final String userNameValue = userNameEditText.getText().toString();
                final String passwordValue = passwordEditText.getText().toString();


                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(RegisterInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RegisterInterface registerInterface = retrofit.create(RegisterInterface.class);
                // Use default converter factory, so parse response body text took http3.ResponseBody object.
                Call<ResponseBody> call = registerInterface.login(userNameValue, passwordValue);
                // Create a Callback object, because we do not set JSON converter, so the return object is ResponseBody be default.
                call.enqueue(new Callback<ResponseBody>() {
                    @Override
                    public void onResponse(Call<ResponseBody> call,
                                           Response<ResponseBody> response) {

                        StringBuffer messageBuffer = new StringBuffer();
                        int statusCode = response.code();
                        if (statusCode == 200) {
                            try {
                                // Get return string.
                                String returnBodyText = response.body().string();
                                // Because return text is a json format string, so we should parse it manually.
                                Gson gson = new Gson();
                                TypeToken<List<RegisterResponse>> typeToken = new
                                        TypeToken<List<RegisterResponse>>() {
                                        };
                                // Get the response data list from JSON string.
                                List<RegisterResponse> registerResponseList =
                                        gson.fromJson(returnBodyText, typeToken.getType());
                                if (registerResponseList != null &&
                                        registerResponseList.size() > 0) {
                                    RegisterResponse registerResponse =
                                            registerResponseList.get(0);
                                    if (registerResponse.isSuccess()) {

                                        messageBuffer.append("Login Success");
                                        Intent intent = new Intent(MainActivity.this,home.class);

                                        startActivity(intent);

                                    } else {
                                        messageBuffer.append("Login failed.");
                                    }
                                }
                            } catch (IOException ex) {
                                Log.e(TAG_RETROFIT_GET_POST, ex.getMessage());
                            }
                        } else {
                            // If server return error.
                            messageBuffer.append("Server return error code is ");
                            messageBuffer.append(statusCode);
                            messageBuffer.append("\r\n\r\n");
                            messageBuffer.append("Error message is ");
                            messageBuffer.append(response.message());
                        }
                        // Show a Toast message.
                        Toast.makeText(getApplicationContext(),
                                messageBuffer.toString(), Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onFailure(Call<ResponseBody> call, Throwable t) {

                        Toast.makeText(getApplicationContext(), t.getMessage(),
                                Toast.LENGTH_LONG).show();
                    }
                });

            }
        });
    }
}