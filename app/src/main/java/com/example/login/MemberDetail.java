package com.example.login;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import static android.Manifest.permission.CAMERA;

public class MemberDetail extends AppCompatActivity {

    private static final int REQUEST_CAMERA = 100;
    private static String[] PERMISSIONS_CAMERA = {CAMERA};
    ImageView  img2,img3,imghome,imgout ;
    EditText edt1 ;
    Button btn1 ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_member_detail);

        edt1 = findViewById(R.id.text_d);
        edt1.getText().toString();
        btn1 = findViewById(R.id.search_edit);

        btn1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Retrofit retrofit = new Retrofit.Builder()
                        .baseUrl(RegisterInterface.BASE_URL)
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();

                RegisterInterface registerInterface = retrofit.create(RegisterInterface.class);
                Call<CustProfile> call = registerInterface.getCust();
                call.enqueue(new Callback<CustProfile>() {
                    @Override
                    public void onResponse(Call<CustProfile> call, Response<CustProfile> response) {

                    }

                    @Override
                    public void onFailure(Call<CustProfile> call, Throwable t) {

                    }
                });

            }
        });



        img2 = findViewById(R.id.scanqr);
        img3 = findViewById(R.id.profile);
        imghome = findViewById(R.id.imageView7);
        imgout = findViewById(R.id.imageView8);

        imghome.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,home.class);
                startActivity(intent);

            }
        });

        imgout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,MainActivity.class);
                startActivity(intent);

            }
        });



        img2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,ScanQR.class);
                startActivity(intent);
            }
        });


        img3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MemberDetail.this,Profile.class);
                startActivity(intent);
                int currentapiVersion = android.os.Build.VERSION.SDK_INT;
                if (currentapiVersion >= android.os.Build.VERSION_CODES.M) {
                    if (checkPermission()) {
                        Toast.makeText(getApplicationContext(), "Permission already granted",
                                Toast.LENGTH_LONG).show();
                    } else {
                        requestPermission(MemberDetail.this);
                    }
                }
            }
        });
    }
    private boolean checkPermission() {
        // Check if we have permission
        return (ContextCompat.checkSelfPermission(
                getApplicationContext(),
                CAMERA) == PackageManager.PERMISSION_GRANTED
        );
    }
    private void requestPermission(Activity activity) {
        // We don't have permission so prompt the user
        ActivityCompat.requestPermissions(
                activity,
                PERMISSIONS_CAMERA,
                REQUEST_CAMERA
        );
    }
}